<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function books(){
        return $this->belongsToMany(Book::class,'categories_has_books','category_id','book_id')
                    ->withTimestamps()
                    ->orderByDesc('id');
    }
}
