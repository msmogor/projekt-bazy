<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        //@todo
    ];

    public function categories(){
        return $this->belongsToMany(Categories::class,'categories_has_books','book_id','category_id')
                    ->withTimestamps();
    }

    public function authors(){
        return $this->belongsToMany(Author::class,'authors_has_books','book_id','author_id')
                    ->withTimestamps();
    }
    public function attributes(){
        return $this->belongsToMany(Attributes::class, 'attributes_has_books','book_id','attribute_id')
                    ->withTimestamps();
    }
    public function carts(){
        return $this->belongsToMany(Cart::class, 'carts_has_books','book_id','cart_id')
            ->withTimestamps();
    }
}
