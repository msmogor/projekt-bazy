<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{
    public function books(){
        return $this->belongsToMany(Book::class,'attributes_has_books','attribute_id','book_id')
                    ->withTimestamps()
                    ->orderByDesc('id');
    }
}
