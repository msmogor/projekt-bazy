<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        //@todo
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function books(){
        return $this->belongsToMany(Book::class,'authors_has_books','author_id','book_id')
                    ->withTimestamps()
                    ->orderByDesc('id');
    }
}
