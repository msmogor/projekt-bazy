<?php

namespace App\Http\Controllers;

use App\Book;
use App\Categories;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $new_category = Categories::all()->random(1)->first();
        $slider = Book::inRandomOrder()->limit(8)->get();
        $flashItem = Book::all()->random(1);

        return view('home', compact('slider','flashItem','new_category'));
    }
}
