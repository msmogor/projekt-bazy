<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;
use App\Book;


class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::all()->toArray();
        $books = Book::with('categories')->paginate(9);
        return view('Books.index',compact('books','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Categories::all()->toArray();
        $book = collect(Book::with('categories')->where('id',$id)->first());

        return view('Books.single',compact('book','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showByAttribute($id){
        $categories = Categories::all()->toArray();
        $books = Book::whereHas('attributes', $filter = function ($query) use($id){
            $query->where('attribute_id', '=', $id);
        })->with(['attributes' => $filter])->paginate(9);

        return view('Books.index',compact('books','categories'));
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showByCategory($id)
    {
        $categories = Categories::all()->toArray();
        $books = Book::whereHas('categories', $filter = function ($query) use($id){
            $query->where('category_id', '=', $id);
        })->with(['categories' => $filter])->paginate(9);

        return view('Books.index',compact('books','categories'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request) {
        $query = $request->search;
        $data = Book::query()->where('name','LIKE',"%{$query}%")->get();
        return response()->json($data);
    }
}
