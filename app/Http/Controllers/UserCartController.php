<?php

namespace App\Http\Controllers;

use App\Book;
use App\Cart as Cart;
use App\User;
use App\CartsHasBooks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UserCartController extends Controller
{
    /**
     * @param Request $request
     * @return Request|mixed
     */
    public function store(Request $request){
        $cart = User::where('id',Auth::id())->get(['cart_id'])->first();
        $find_attachment = CartsHasBooks::where([['cart_id', $cart['cart_id']],['book_id',$request->data['book_id']]])->first();
        if($find_attachment !== null){
            $count = isset($request->count)?$request->count:1;
            $find_attachment->count+=$count;
            $find_attachment->save();
        }else{
            $book = collect(Book::where('id',$request->data['book_id'])->first());
            $cart = Cart::where('id',$cart['cart_id'])->first();
            $cart->books()->attach($book['id']);
        }
        $book = collect(Book::where('id',$request->data['book_id'])->first());
        $book['link'] = route('books.show',$book['id']);
        $book['destroy_link'] = route('cart.destroy',$book['id']);
        $book['img_link'] = url('/').'/'.$book['img'];
        $book['count'] = collect(CartsHasBooks::select('count')->where([['cart_id', $cart['cart_id']],['book_id',$request->data['book_id']]])->pluck('count'));
        //$book['img_link'] = 'http://projekt_bazy.local/images/item-cart-01.jpg';
        return collect($book);
    }

    /**
     *
     */
    public function destroy($id){
        $cart = User::where('id',Auth::id())->get(['cart_id'])->first();
        $cart = Cart::where('id',$cart['cart_id'])->first();

        $cart->books()->detach($id);
        return redirect()->back();
    }
}
