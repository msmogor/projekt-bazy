<?php

namespace App\Http\Controllers;

use App\Favorites;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FavoritesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id =  Auth::id();
        $items = DB::table('books')
            ->select('*')
            ->join('favorites', 'favorites.book_id', '=', 'books.id')
            ->where('favorites.user_id', $id)
            ->paginate(15);
        return view('Users.favorites', ['favorites'=>$items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user;
        $book = $request->book;

        $exists = Favorites::where([['user_id','=',$user],['book_id','=',$book]])->exists();
        if(!$exists) {
            $item = new Favorites();
            $item->user_id = $user;
            $item->book_id = $book;
            $item->save();
            return response()->json(['data'=>_i('Przedmiot został pomyślnie dodany do ulubionych!')]);
        }else return response()->json(
            [
                'header'=>_i('Przedmiot znajduje się już ulubionych!'),
                'text'=>_i('Przejdź do sekcji ulubione w swoim profilu'),
            ]
        )->setStatusCode(403);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Favorites  $favorites
     * @return \Illuminate\Http\Response
     */
    public function show(Favorites $favorites)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Favorites  $favorites
     * @return \Illuminate\Http\Response
     */
    public function edit(Favorites $favorites)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Favorites  $favorites
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favorites $favorites)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Favorites  $favorites
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $item = Favorites::find($request->id);
        if($item){
            $item->delete();
            return redirect()->back()->with(['success'=>_i('Pomyślnie usunięto przedmiot z ulubionych!')]);
        }else return redirect()->route('home');
    }
}
