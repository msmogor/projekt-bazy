<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function user(){
        return $this->hasOne('App\User','cart_id');
    }

    public function books(){
        return $this->belongsToMany(Book::class,'carts_has_books','cart_id','book_id')
            ->withTimestamps()
            ->orderByDesc('id');
    }
}
