��    %      D  5   l      @  >   A     �     �  C   �  
   �     �     �  
   �     �     �                    *     1      =  	   ^     h     n     u     ~     �     �     �     �     �     �     �     �     �       K     U   g     �     �     �  �  �  C   �     �     �  R   �     J	  	   Y	  	   c	  	   m	  	   w	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     
     
     
     *
  "   :
     ]
     f
     l
     �
     �
     �
  i   �
  i   E  &   �     �     �         "   	      
                                                                      $                                                %                              #      !       A fresh verification link has been sent to your email address. Author Authors Before proceeding, please check your email for a verification link. Birth date Book Books Categories Category City Confirm Password Country E-Mail Address Gender Home Number If you did not receive the email Last name Login Logout Modified Name Password Register Reset Password Send Password Reset Link Showing Street Toggle navigation Verify Your Email Address View all books View all categories You have %s Books in your database. Click on button below to view all books You have %s Categories in your database. Click on button below to view all categories click here to request another of results Project-Id-Version: MultilanguageLaravelApplication
POT-Creation-Date: 2019-03-16 23:12+0100
PO-Revision-Date: 2019-03-16 23:16+0100
Last-Translator: James Translator <james@translations.colm>
Language-Team: James Translator <james@translations.colm>
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-KeywordsList: _;__;_i;_s;gettext;_n:1,2;ngettext:1,2;dgettext:2
X-Poedit-Basepath: ../../../../..
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: app
X-Poedit-SearchPath-1: app/Http
X-Poedit-SearchPath-2: app/Console
X-Poedit-SearchPath-3: storage/framework/messages
X-Poedit-SearchPath-4: database/seeds
 Świeży link weryfikacyjny został wysłany na Twój adres e-mail. Autor Autorzy Zanim przejdziesz dalej, sprawdź pocztę e-mail, aby uzyskać link weryfikacyjny. Data urodzenia Książka Książki Kategorie Kategoria Miasto Potwierdź hasło Kraj Adres E-mail Płeć Numer domu Jeśli nie dostałeś e-maila Nazwisko Zaloguj się Wyloguj się Zmodyfikowany Imie Hasło Zarejestruj się Zresetuj hasło Wyślij link do resetowania hasła Pokazuje Ulica Przełącz nawigację Potwierdź swój adres email Wyświetl wszystkie książki Wyświetl wszystkie kategorie Masz % s książek w swojej bazie danych. Kliknij przycisk poniżej, aby wyświetlić wszystkie książki Masz % s kategorii w swojej bazie danych. Kliknij przycisk poniżej, aby wyświetlić wszystkie kategorie kliknij tutaj, aby poprosić o kolejne z wyników 