@extends('layouts.app')
@section('content')

    <!-- Content page -->
    <section class="bgwhite p-t-55 p-b-65">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
                    <div class="leftbar p-r-20 p-r-0-sm">
                        <!--  -->
                        <h4 class="m-text14 p-b-7">
                            {{_i('Kategorie')}}
                        </h4>

                        <ul class="p-b-54">
                            <li class="p-t-4">
                                <a href="{{route('books.index')}}" class="s-text13 active1" style="{{Request::is('books') ? 'font-weight:bold;color:#e3342f' : ''}}">
                                    {{_i('Wszystkie')}}
                                </a>
                            </li>
                            @foreach($categories as $category)
                                <li class="p-t-4">
                                    <a href="{{route('books.category',$category['id'])}}" class="s-text13" style="{{Request::is('books/category/'.$category['id']) ? 'font-weight:bold;color:#e3342f' : ''}}">
                                        {{$category['name']}}
                                    </a>
                                </li>
                            @endforeach

                        </ul>
                        <!--  -->
                        <h4 class="m-text14 p-b-32">
                            {{_i('Filtry')}}
                        </h4>

                        <div class="filter-price p-t-22 p-b-50 bo3">
                            <div class="m-text15 p-b-17">
                                {{_i('Cena')}}
                            </div>

                            <div class="wra-filter-bar">
                                <div id="filter-bar"></div>
                            </div>

                            <div class="flex-sb-m flex-w p-t-16">
                                <div class="w-size11">
                                    <!-- Button -->
                                    <button class="flex-c-m size4 bg7 bo-rad-15 hov1 s-text14 trans-0-4">
                                        {{_i('Filtruj')}}
                                    </button>
                                </div>

                                <div class="s-text3 p-t-10 p-b-10">
                                    {{_i('Cena')}}: $<span id="value-lower">610</span> - $<span id="value-upper">980</span>
                                </div>
                            </div>
                        </div>

                        <div class="search-product pos-relative bo4 py-3">
                            <select class="s-text7 size6 p-l-23 p-r-50 form-control" type="text" name="search-product" id="custom-3">
                                <option>{{_i('Wyszukaj produkt...')}}</option>
                            </select>

                            <button class="flex-c-m size5 ab-r-m color2 color0-hov trans-0-4">
                                <i class="fs-12 fa fa-search" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
                    <div class="flex-sb-m flex-w p-b-35">
                        <div class="flex-w">
                            <div class="rs2-select2 bo4 of-hidden w-size12 m-t-5 m-b-5 m-r-10">
                                <select class="selection-2" id="custom-1" name="sorting">
                                    <option>{{_i('Domyślne sortowanie')}}</option>
                                    <option>{{_i('Popularne')}}</option>
                                    <option>{{_i('Cena: Od najtańszej')}}</option>
                                    <option>{{_i('Cena: Od najdroższej')}}</option>
                                </select>
                            </div>

                            <div class="rs2-select2 bo4 of-hidden w-size12 m-t-5 m-b-5 m-r-10">
                                <select class="selection-2" id="custom-2" name="sorting">
                                    <option>{{_i('Cena')}}</option>
                                    <option>0 - 10zł</option>
                                    <option>10zł - 30zł</option>
                                    <option>30zł - 150zł</option>
                                    <option>150zł +</option>

                                </select>
                            </div>
                        </div>

                        <span class="s-text8 p-t-5 p-b-5">
							{{_i('Showing')}} 1–9 {{_i('of')}} {{$books->total()}} {{_i('results')}}
						</span>
                    </div>
                    <!-- Product -->
                    <div class="row">
                        @foreach($books as $book)
                        <div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
                            <!-- Block2 -->
                            <div class="block2">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                    <img src="{{url('/images/new').'/book-placeholder.jpg'}}" alt="IMG-PRODUCT">

                                    <div class="block2-overlay trans-0-4">
                                        <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                            <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                            <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                        </a>

                                        <div class="block2-btn-addcart w-size1 trans-0-4">
                                            <!-- Button -->
                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="font-size: 0.8rem">
                                                {{_i('Dodaj do koszyka')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="block2-txt p-t-20">
                                    <a href="{{route('books.show',$book['id'])}}" class="block2-name dis-block s-text3 p-b-5">
                                        {{$book['name']}}
                                    </a>
                                    <input type="hidden" id="idProduct" value="{{$book['id']}}">
                                    <span class="block2-price m-text6 p-r-5">
										{{$book['price'].' zł'}}
									</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- Pagination -->
                    @include('layouts.pagination', ['paginator' => $books])
                </div>
            </div>
        </div>
    </section>
    <!-- Container Selection -->
    <div id="dropDownSelect1"></div>
    <div id="dropDownSelect2"></div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{url('/vendor/jquery').'/jquery-3.2.1.min.js'}}"></script>

    <!--===============================================================================================-->
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/animsition/js/animsition.min.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/bootstrap/js/popper.js'}}"></script>
    <script type="text/javascript" src="{{url('/vendor').'/bootstrap/js/bootstrap.min.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/select2/select2.js'}}"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });

        $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect2')
        });

    </script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/daterangepicker/moment.min.js'}}"></script>
    <script type="text/javascript" src="{{url('/vendor').'/daterangepicker/daterangepicker.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/slick/slick.min.js'}}"></script>
    <script type="text/javascript" src="{{url('/js').'/slick-custom.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor'.'/sweetalert/sweetalert.min.js')}}"></script>
    <script type="text/javascript">
        $('.block2-btn-addcart').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            var idProduct = $(this).parent().parent().parent().find('#idProduct').attr('value');
            $(this).on('click', function(){
                axios.post('/cart/store',{
                    data: {
                        book_id: idProduct,
                    },
                }).then((response)=>{
                    let items_count = response.data.count[0];
                    if(items_count===undefined)
                        items_count=1;
                    let items_count_cart = parseInt($('.header-icons-noti').html());
                    if($('#book_'+response.data.id).length>0){
                        $('#book_'+response.data.id).find('.header-cart-item-info').html(items_count+' x '+response.data.price+'zł');
                        $('.header-icons-noti').html(++items_count_cart);
                    }else {
                        $('#appendable').prepend('' +
                            '<li class="header-cart-item" id="book_' + response.data.id + '">' +
                            '<a href="'+response.data.destroy_link+'"><div class="header-cart-item-img">' +
                            '<img src="' + response.data.img_link + '" alt="IMG">' +
                            '</div></a>' +
                            '<div class="header-cart-item-txt">' +
                            '<a href="' + response.data.link + '" class="header-cart-item-name">' +
                            response.data.name +
                            '</a>' +
                            '<span class="header-cart-item-info">' +
                            items_count + ' x ' + response.data.price + 'zł' +
                            '</span>' +
                            '</div>' +
                            ' </li>'
                        );
                        $('.header-icons-noti').html(++items_count_cart);
                    }
                    }
                ).catch((error)=>{

                });
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();

            $(this).on('click', function(){
                // swal(nameProduct, "został dodany do listy życzeń !", "success");

                let itemHref = $(this).parent().parent().parent().find('.block2-name');
                let id = itemHref[0].pathname;
                id = id.replace('/books/', '');

                $.ajax({
                    type: "POST",
                    url: '/api/addFav',
                    data: {book:id, user:`{{Auth::id()}}`},
                    success: function (response) {
                        const msg = response.data;
                        swal(nameProduct, msg, "success");
                    },
                    error: function(response) {
                        swal(response.responseJSON.header, response.responseJSON.text, "error");
                    }
                });

            });
        });
    </script>

    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/noui/nouislider.min.js'}}"></script>
    <script type="text/javascript">
        /*[ No ui ]
        ===========================================================*/
        var filterBar = document.getElementById('filter-bar');

        noUiSlider.create(filterBar, {
            start: [ 50, 200 ],
            connect: true,
            range: {
                'min': 50,
                'max': 200
            }
        });

        var skipValues = [
            document.getElementById('value-lower'),
            document.getElementById('value-upper')
        ];

        filterBar.noUiSlider.on('update', function( values, handle ) {
            skipValues[handle].innerHTML = Math.round(values[handle]) ;
        });
    </script>
    <script>
        $('#custom-1').select2({
            placeholder: "Wybierz kategorie",
        });
        if ($('#custom-1').hasClass("select2-hidden-accessible")) {
            $('#custom-1').on('select2:select', function (e) {
                console.log('select event',e.target.value);
            });
        }


        $('#custom-2').select2({
            placeholder: "Wybierz filtrowanie",
        });
        if ($('#custom-2').hasClass("select2-hidden-accessible")) {
            $('#custom-2').on('select2:select', function (e) {
                console.log('select event',e.target.value);
            });
        }

        $("#custom-3").select2({
            ajax: {
                method: "POST",
                url: "/api/book/search",
                dataType: 'json',
                delay: 50,
                data: function (params) {
                    return {
                        search: params.term, // search term
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data,
                    };
                },
                cache: false
            },
            // placeholder: 'Search products...',
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 2,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });

        function formatRepo (repo) {
            // console.log(repo);
            if (repo.loading) {
                return repo.text;
            }

            var markup = "<div class='select2-result-repository clearfix pl-0'>" +
                "<div class='select2-result-repository__title pl-0'>" + repo.name + "</div>"
            + "</div>";

            return markup;
        }

        function formatRepoSelection (repo) {
            return repo.full_name || repo.text;
        }



        if ($('#custom-3').hasClass("select2-hidden-accessible")) {
            $('#custom-3').on('select2:select', function (e) {
                window.location = '/books/'+e.target.value;
            });
        }

    </script>
    @endsection
