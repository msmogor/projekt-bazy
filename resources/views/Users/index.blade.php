@extends('layouts.app')
@section('content')

    <section class="py-2">
        <div class="container py-3">
            <p class="h1 text-center pt-2">{{_i('Mój profil')}}</p>
        </div>
        @include('Users.UserMenu')
        <div class="container mt-4 pb-3">
            <div class="py-2">
                <div class="row no-gutters px-5">
                    <div class="col-md-4 m-auto">
                        <img alt="" style="width:600px;border:none" title="" class="img-circle img-thumbnail isTooltip rounded-lg" src="images/new/flash-1.jpeg">
                        <ul title="Ratings" class="list-inline ratings text-center">
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 p-3 m-auto">
                        <div class="row no-gutter py-3">
                            <div class="col-6">
                                <p class="h4">{{_i('Information')}}</p>
                            </div>
                            <div class="col-6">
                                <a type="button" href="{{route('user.edit')}}" class="btn btn-sm btn-dark">{{_i('Edytuj profil')}}</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-condensed table-responsive table-user-information borderless">
                                <tbody>
                                <tr>
                                    <td>
                                        <strong>
                                            <span class="glyphicon glyphicon-user text-black"></span>
                                            {{_i('Name')}}
                                        </strong>
                                    </td>
                                    <td class="text-black">
                                        {{$user['name']}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            <span class="glyphicon glyphicon-cloud text-black"></span>
                                            {{_i('Last name')}}
                                        </strong>
                                    </td>
                                    <td class="text-black">
                                        {{$user['last_name']}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            <span class="glyphicon glyphicon-bookmark text-black"></span>
                                            {{_i('Email')}}
                                        </strong>
                                    </td>
                                    <td class="text-black">
                                        {{$user['email']}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            <span class="glyphicon glyphicon-eye-open text-black"></span>
                                            {{_i('Country')}}
                                        </strong>
                                    </td>
                                    <td class="text-black">
                                        {{$user['country']}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            <span class="glyphicon glyphicon-envelope text-black"></span>
                                            {{_i('City')}}
                                        </strong>
                                    </td>
                                    <td class="text-black">
                                        {{$user['city']}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            <span class="glyphicon glyphicon-calendar text-black"></span>
                                            {{_i('Street')}}
                                        </strong>
                                    </td>
                                    <td class="text-black">
                                        {{$user['street']}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            <span class="glyphicon glyphicon-cloud text-black"></span>
                                            {{_i('Gender')}}
                                        </strong>
                                    </td>
                                    <td class="text-black">
                                        {{$user['home_number']}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            <span class="glyphicon glyphicon-cloud text-black"></span>
                                            {{_i('Birth date')}}
                                        </strong>
                                    </td>
                                    <td class="text-black">
                                        {{$user['gender']}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            <span class="glyphicon glyphicon-calendar text-black"></span>
                                            {{_i('Modified')}}
                                        </strong>
                                    </td>
                                    <td class="text-black">
                                        {{$user['updated_at']}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        {{--<a href="{{route('user.edit')}}"><button type="button" class="btn btn-dark">Edit</button></a>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .inf-content{
            /*border:1px solid #DDDDDD;*/
            /*-webkit-border-radius:10px;*/
            /*-moz-border-radius:10px;*/
            /*border-radius:10px;*/
            /*box-shadow: 7px 7px 7px rgba(0, 0, 0, 0.3);*/
        }

        .borderless td, .borderless th {
            border: none;
            padding: 0 15px;
        }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{url('/vendor/jquery').'/jquery-3.2.1.min.js'}}"></script>

    <!--===============================================================================================-->
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/animsition/js/animsition.min.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/bootstrap/js/popper.js'}}"></script>
    <script type="text/javascript" src="{{url('/vendor').'/bootstrap/js/bootstrap.min.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/select2/select2.min.js'}}"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });

        $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect2')
        });
    </script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/daterangepicker/moment.min.js'}}"></script>
    <script type="text/javascript" src="{{url('/vendor').'/daterangepicker/daterangepicker.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/slick/slick.min.js'}}"></script>
    <script type="text/javascript" src="{{url('/js').'/slick-custom.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor'.'/sweetalert/sweetalert.min.js')}}"></script>
    <script type="text/javascript">
        $('.block2-btn-addcart').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to wishlist !", "success");
            });
        });
    </script>

    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/noui/nouislider.min.js'}}"></script>
    <script type="text/javascript">
        /*[ No ui ]
        ===========================================================*/
        var filterBar = document.getElementById('filter-bar');

        noUiSlider.create(filterBar, {
            start: [ 50, 200 ],
            connect: true,
            range: {
                'min': 50,
                'max': 200
            }
        });

        var skipValues = [
            document.getElementById('value-lower'),
            document.getElementById('value-upper')
        ];

        filterBar.noUiSlider.on('update', function( values, handle ) {
            skipValues[handle].innerHTML = Math.round(values[handle]) ;
        });
    </script>
@endsection
