<div class="container py-0">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link {{Request::is('user') ? 'active font-weight-bold text-danger' : ''}}" href="{{route('user.index')}}">{{_i('Mój profil')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">{{_i('Zamówienia')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{Request::is('user/favorites') ? 'active font-weight-bold text-danger' : ''}}" href="{{route('user.favorites')}}">{{_i('Ulubione')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#">{{_i('Wypożyczenia')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#">{{_i('Historia')}}</a>
        </li>
    </ul>
</div>