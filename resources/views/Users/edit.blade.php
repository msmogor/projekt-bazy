@extends('layouts.app')
@section('content')

    <section class="bgwhite p-t-55 p-b-65">
        <div class="container bootstrap snippet">
            <div class="panel-body inf-content">
                <div class="row">
                    <div class="col-md-4">
                        <img alt="" style="width:600px;border:0px;" title="" class="img-circle img-thumbnail isTooltip" src="{{url('images/').'/new/flash-1.jpeg'}}">
                        <ul title="Ratings" class="list-inline ratings text-center">
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <strong>Information</strong><br>
                        <div class="table-responsive">
                            <table class="table table-condensed table-responsive table-user-information">
                                <form method="POST" action="{{route('user.update')}}">
                                    @csrf
                                    <tbody>
                                    <tr>
                                        <td>
                                            <strong>
                                                <span class="glyphicon glyphicon-user  text-black"></span>
                                                {{__('Name')}}
                                            </strong>
                                        </td>
                                        <td class="text-black">
                                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user['name'] }}"  autofocus>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>
                                                <span class="glyphicon glyphicon-cloud text-black"></span>
                                                {{__('Last name')}}
                                            </strong>
                                        </td>
                                        <td class="text-black">
                                            <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ $user['last_name'] }}"  autofocus>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <strong>
                                                <span class="glyphicon glyphicon-bookmark text-black"></span>
                                                {{__('Email')}}
                                            </strong>
                                        </td>
                                        <td class="text-black">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user['email'] }}" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>
                                                <span class="glyphicon glyphicon-eye-open text-black"></span>
                                                {{__('Country')}}
                                            </strong>
                                        </td>
                                        <td class="text-black">
                                            <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{$user['country']}}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>
                                                <span class="glyphicon glyphicon-envelope text-black"></span>
                                                {{__('City')}}
                                            </strong>
                                        </td>
                                        <td class="text-black">
                                            <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{$user['city']}}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>
                                                <span class="glyphicon glyphicon-calendar text-black"></span>
                                                {{__('Street')}}
                                            </strong>
                                        </td>
                                        <td class="text-black">
                                            <input id="street" type="text" class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" name="street" value="{{$user['street']}}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>
                                                <span class="glyphicon glyphicon-calendar text-black"></span>
                                                {{__('Home number')}}
                                            </strong>
                                        </td>
                                        <td class="text-black">
                                            <input id="home_number" type="text" class="form-control{{ $errors->has('home_number') ? ' is-invalid' : '' }}" value="{{$user['home_number']}}" name="home_number">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>
                                                <span class="glyphicon glyphicon-cloud text-black"></span>
                                                {{__('Gender')}}
                                            </strong>
                                        </td>
                                        <td class="text-black">
                                            <input id="gender" type="text" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" value="{{$user['gender']}}" name="gender">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>
                                                <span class="glyphicon glyphicon-calendar text-black"></span>
                                                {{__('Birth date')}}
                                            </strong>
                                        </td>
                                        <td class="text-black">
                                            <input id="birth_date" type="date" class="form-control{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" value="{{$user['birth_date']}}" name="birth_date">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="submit" class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4" value="{{__('Save')}}">
                                        </td>
                                    </tr>
                                    </tbody>
                                </form>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .inf-content{
            border:1px solid #DDDDDD;
            -webkit-border-radius:10px;
            -moz-border-radius:10px;
            border-radius:10px;
            box-shadow: 7px 7px 7px rgba(0, 0, 0, 0.3);
        }
    </style>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{url('/vendor/jquery').'/jquery-3.2.1.min.js'}}"></script>

    <!--===============================================================================================-->
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/animsition/js/animsition.min.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/bootstrap/js/popper.js'}}"></script>
    <script type="text/javascript" src="{{url('/vendor').'/bootstrap/js/bootstrap.min.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/select2/select2.min.js'}}"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });

        $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect2')
        });
    </script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/daterangepicker/moment.min.js'}}"></script>
    <script type="text/javascript" src="{{url('/vendor').'/daterangepicker/daterangepicker.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/slick/slick.min.js'}}"></script>
    <script type="text/javascript" src="{{url('/js').'/slick-custom.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor'.'/sweetalert/sweetalert.min.js')}}"></script>
    <script type="text/javascript">
        $('.block2-btn-addcart').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to wishlist !", "success");
            });
        });
    </script>

    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/noui/nouislider.min.js'}}"></script>
    <script type="text/javascript">
        /*[ No ui ]
        ===========================================================*/
        var filterBar = document.getElementById('filter-bar');

        noUiSlider.create(filterBar, {
            start: [ 50, 200 ],
            connect: true,
            range: {
                'min': 50,
                'max': 200
            }
        });

        var skipValues = [
            document.getElementById('value-lower'),
            document.getElementById('value-upper')
        ];

        filterBar.noUiSlider.on('update', function( values, handle ) {
            skipValues[handle].innerHTML = Math.round(values[handle]) ;
        });
    </script>
@endsection
