@extends('layouts.app')
@section('content')
    <section class="py-2">
        <div class="container py-3">
            <p class="h1 text-center pt-2">{{_i('Mój profil')}}</p>
        </div>
       @include('Users.UserMenu')
        <div class="container mt-4 pb-3">
            <div class="py-2">
                @if (session('success'))
                    <div class="alert alert-success py-3 my-3 mb-4 font-weight-bold">
                        {{ session('success') }}
                    </div>
                @endif
                <h3 class="pl-3 pb-3 font-weight-bold">{{_i('Moje ulubione produkty')}}</h3>
                <div class="row no-gutters my-3 text-center d-flex align-items-center font-weight-bold">
                    <div class="col-md-1">
                        LP.
                    </div>
                    <div class="col-md-3">
                       {{_i('Obrazek')}}
                    </div>
                    <div class="col-md-3">
                        {{_i('Nazwa')}}
                    </div>
                    <div class="col-md-2">
                        {{_i('Cena')}}
                    </div>
                    <div class="col-md-2">
                        {{_i('Data dodania')}}
                    </div>
                    <div class="col-md-1">

                    </div>
                </div>
                @foreach ($favorites as $i=>$favorite)
                <div class="row no-gutters my-3 text-center d-flex align-items-center">
                    <div class="col-md-1">
                        <span>{{$i+1}}</span>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('books.show',$favorite->book_id)}}" class="d-block">
                            <img src="{{url($favorite->img)}}" class="img-fluid border rounded" alt="" style="height:150px">
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('books.show',$favorite->book_id)}}" class="d-block">{{$favorite->name}}</a>
                    </div>
                    <div class="col-md-2">
                        {{$favorite->price}} zł
                    </div>
                    <div class="col-md-2">
                        {{date('d-m-Y', strtotime($favorite->created_at))}}
                    </div>
                    <div class="col-md-1">
                        <a href="{{route('favorites.delete',['id'=>$favorite->id])}}" class="btn btn-outline-danger" role="button">{{_i('Usuń')}}</a>
                    </div>
                </div>
                @endforeach


                {{ $favorites->links() }}
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{url('/vendor/jquery').'/jquery-3.2.1.min.js'}}"></script>

    <!--===============================================================================================-->
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/animsition/js/animsition.min.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/bootstrap/js/popper.js'}}"></script>
    <script type="text/javascript" src="{{url('/vendor').'/bootstrap/js/bootstrap.min.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/select2/select2.min.js'}}"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });

        $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect2')
        });
    </script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/daterangepicker/moment.min.js'}}"></script>
    <script type="text/javascript" src="{{url('/vendor').'/daterangepicker/daterangepicker.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/slick/slick.min.js'}}"></script>
    <script type="text/javascript" src="{{url('/js').'/slick-custom.js'}}"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor'.'/sweetalert/sweetalert.min.js')}}"></script>
    <script type="text/javascript">
        $('.block2-btn-addcart').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to wishlist !", "success");
            });
        });
    </script>

    <!--===============================================================================================-->
    <script type="text/javascript" src="{{url('/vendor').'/noui/nouislider.min.js'}}"></script>
    <script type="text/javascript">
        /*[ No ui ]
        ===========================================================*/
        var filterBar = document.getElementById('filter-bar');

        noUiSlider.create(filterBar, {
            start: [ 50, 200 ],
            connect: true,
            range: {
                'min': 50,
                'max': 200
            }
        });

        var skipValues = [
            document.getElementById('value-lower'),
            document.getElementById('value-upper')
        ];

        filterBar.noUiSlider.on('update', function( values, handle ) {
            skipValues[handle].innerHTML = Math.round(values[handle]) ;
        });
    </script>
@endsection
