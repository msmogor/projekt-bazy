@section('footer')
    <!-- Back to top -->
    <div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
    </div>
    <!-- Footer -->
    <footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
        <div class="flex-w p-b-90">
            <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
                <h4 class="s-text12 p-b-30">
                    {{_i('Skontaktuj się z nami!')}}
                </h4>

                <div>
                    <p class="s-text7 w-size27">
                        Zespół szkół łączności im. Mikołaja Kopernika w Poznaniu
                    </p>

                    <div class="flex-m p-t-30">
                        <a href="#" class="fs-18 color1 p-r-20 fa fa-facebook"></a>
                        <a href="#" class="fs-18 color1 p-r-20 fa fa-instagram"></a>
                        <a href="#" class="fs-18 color1 p-r-20 fa fa-pinterest-p"></a>
                        <a href="#" class="fs-18 color1 p-r-20 fa fa-snapchat-ghost"></a>
                        <a href="#" class="fs-18 color1 p-r-20 fa fa-youtube-play"></a>
                    </div>

                </div>
            </div>

            <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                <h4 class="s-text12 p-b-30">
                    {{_i('Kategorie')}}
                </h4>

                <ul>
                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            {{_i('Fantasy')}}
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            {{_i('Literatura')}}
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            {{_i('Pomoce naukowe')}}
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            {{_i('Kryminał')}}
                        </a>
                    </li>
                </ul>
            </div>

            <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                <h4 class="s-text12 p-b-30">
                    {{_i('Przydatne linki')}}
                </h4>

                <ul>
                    <li class="p-b-9">
                        <a href="{{route('about')}}" class="s-text7">
                            {{_i('O nas')}}
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="{{route('contact')}}" class="s-text7">
                            {{_i('Kontakt')}}
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="{{route('books.index')}}" class="s-text7">
                            {{_i('Sklep')}}
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="https://zslpoznan.mobidziennik.pl/dziennik/" target="_BLANK" class="s-text7">
                            {{_i('Mobidziennik')}}
                        </a>
                    </li>
                </ul>
            </div>

            <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                <h4 class="s-text12 p-b-30">
                    {{_i('Pomoc')}}
                </h4>

                <ul>
                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            {{_i('Przesyłki')}}
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            {{_i('Zwroty')}}
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            {{_i('Lektury szkolne')}}
                        </a>
                    </li>

                    <li class="p-b-9">
                        <a href="#" class="s-text7">
                            {{_i('FAQ')}}
                        </a>
                    </li>
                </ul>
            </div>

            <div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
                <h4 class="s-text12 p-b-30">
                    {{_i('Newsletter')}}
                </h4>

                <form>
                    <div class="effect1 w-size9">
                        <input class="s-text7 bg6 w-full p-b-5" type="text" name="email" placeholder="Email">
                        <span class="effect1-line"></span>
                    </div>

                    <div class="w-size2 p-t-20">
                        <!-- Button -->
                        <button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
                            {{_i('Subskrybuj')}}
                        </button>
                    </div>

                </form>
            </div>
        </div>

        <div class="t-center p-l-15 p-r-15">
            <div class="t-center s-text8 p-t-20">
                Copyright © 2018 Hubert Lipiński and Michał Smogór
            </div>
        </div>
    </footer>

@endsection
