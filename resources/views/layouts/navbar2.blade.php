@section('navbar')
    <header class="header1">
        <!-- Header desktop -->
        <div class="container-menu-header">
            <div class="topbar">
                <div class="topbar-social">
                    <a href="#" class="topbar-social-item fa fa-facebook"></a>
                    <a href="#" class="topbar-social-item fa fa-instagram"></a>
                    <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                    <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                    <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                </div>

                <span class="topbar-child1">
					{{_i('Największa baza ksiażek online!')}}
				</span>

                <div class="topbar-child2">
					<span class="topbar-email">
						{{_i('Zmień język')}}
					</span>

                    <div class="topbar-language rs1-select2 px-2" style="font-size: 1.1rem">
                        {{--@foreach(Config::get('laravel-gettext.supported-locales') as $locale=>$name)--}}
                            {{--<a class="topbar-email" style="{{LaravelGettext::getLocale() == $locale ? 'font-weight: bold; color: #e3342f' : ''}}" href="/lang/{{$locale}}">{{$name}}</a>--}}
                        {{--@endforeach--}}
                        <a class="topbar-email" style="{{LaravelGettext::getLocale() == 'pl_PL' ? 'font-weight: bold; color: #e3342f' : ''}}" href="/lang/pl_PL">{{_i('Polski')}}</a>
                        <a class="topbar-email" style="{{LaravelGettext::getLocale() == 'en_US' ? 'font-weight: bold; color: #e3342f' : ''}}" href="/lang/en_US">{{_i('Angielski')}}</a>
                    </div>
                </div>
            </div>
            <div class="wrap_header">
                <!-- Logo -->
                <a href="{{route('home')}}" class="logo">
                    {{--<img src="images/icons/logo.png" alt="IMG-LOGO">--}}
                    <h1>{{ config('app.name', 'Laravel') }}</h1>
                </a>

                <!-- Menu -->
                <div class="wrap_menu">
                    <nav class="menu">
                        <ul class="main_menu">
                            <li class="{{Request::is('/') ? 'sale-noti' : ''}}">
                                <a href="{{route('home')}}">{{_i('Strona główna')}}</a>
                            </li>

                            <li class="{{Request::is('books*') ? 'sale-noti' : ''}}">
                                <a href="{{route('books.index')}}">{{_i('Sklep')}}</a>
                                <ul class="sub_menu">
                                    @foreach((App\Attributes::all()->toArray()) as $attribute)
                                        <li><a href="{{route('books.attributes', $attribute['id'])}}">{{_i($attribute['name'])}}</a></li>
                                    @endforeach

                                </ul>
                            </li>
                            <li class="{{Request::is('about') ? 'sale-noti' : ''}}">
                                <a href="{{route('about')}}">{{_i('O nas')}}</a>
                            </li>

                            <li class="{{Request::is('contact') ? 'sale-noti' : ''}}">
                                <a href="{{route('contact')}}">{{_i('Kontakt')}}</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <!-- Header Icon -->
                <div class="header-icons">
                    <!-- <a href="#" class="header-wrapicon1 dis-block">
                        <img src="images/icons/icon-header-01.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
                    </a> -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ _i('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ _i('Register') }}</a>
                            </li>
                        @endif
                    @else
                        @if(Voyager::can('browse_admin')===true)
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('user.index')}}">{{Auth::user()->name}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('voyager.dashboard')}}">{{__('Admin panel')}}</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('user.index')}}">{{Auth::user()->name}}</a>
                            </li>
                        @endif
                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            {{ _i('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
                    <span class="linedivide1"></span>

                    <div class="header-wrapicon2">
                        @if(Auth::id())
                        <img src="{{url('/images/icons').'/icon-header-02.png'}}" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        <span class="header-icons-noti">{{\App\Cart::with('books')->where('id',Auth::id())->first()->books()->sum('count')}}</span>
                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <ul id="appendable" class="header-cart-wrapitem">
                                @foreach((\App\Cart::with('books')->where('id',Auth::id())->first()->books) as $book)
                                    <li class="header-cart-item" id="book_{{$book['id']}}">
                                        <a href="{{route('cart.destroy',$book['id'])}}"><div class="header-cart-item-img delete_book">
                                            <img src="{{url('/').'/'.$book['img']}}" alt="IMG" class="">
                                        </div></a>

                                        <div class="header-cart-item-txt">
                                            <a href="{{route('books.show',$book['id'])}}" class="header-cart-item-name">
                                                {{$book['name']}}
                                            </a>

                                            <span class="header-cart-item-info">
											{{$book->pivot->where('book_id',$book['id'])->pluck('count')->first()}} x {{$book['price']}}zł
										</span>
                                        </div>
                                    </li>
                                @endforeach
                            <div class="header-cart-total">
                                Total: {{'ok'}}
                            </div>
                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        View Cart
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Check Out
                                    </a>
                                </div>
                            </div>
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- Header Mobile -->
        <div class="wrap_header_mobile">
            <!-- Logo moblie -->
            <a href="{{route('home')}}" class="logo-mobile">
                <p class="h4 m-0 p-0">{{ config('app.name', 'Laravel') }}</p>
            </a>

            <!-- Button show menu -->
            <div class="btn-show-menu">
                <!-- Header Icon mobile -->
                <div class="header-icons-mobile">
                    <a href="#" class="header-wrapicon1 dis-block">
                        <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                    </a>

                    <span class="linedivide2"></span>

                    <div class="header-wrapicon2">
                        <img src="images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        <span class="header-icons-noti">0</span>


                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <ul class="header-cart-wrapitem">
                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        <img src="images/item-cart-01.jpg" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            White Shirt With Pleat Detail Back
                                        </a>

                                        <span class="header-cart-item-info">
											1 x $19.00
										</span>
                                    </div>
                                </li>

                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        <img src="images/item-cart-02.jpg" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            Converse All Star Hi Black Canvas
                                        </a>

                                        <span class="header-cart-item-info">
											1 x $39.00
										</span>
                                    </div>
                                </li>

                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        <img src="images/item-cart-03.jpg" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            Nixon Porter Leather Watch In Tan
                                        </a>

                                        <span class="header-cart-item-info">
											1 x $17.00
										</span>
                                    </div>
                                </li>
                            </ul>

                            <div class="header-cart-total">
                                Total: $75.00
                            </div>

                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        View Cart
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Check Out
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
                </div>
            </div>
        </div>

        <!-- Menu Mobile -->
        <div class="wrap-side-menu" >
            <nav class="side-menu">
                <ul class="main-menu">
                    <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<span class="topbar-child1">
							{{_i('Największa baza ksiażek online!')}}
						</span>
                    </li>

                    <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
                        <div class="topbar-child2-mobile">
							<span class="topbar-email">
								{{_i('Zmień język')}}
							</span>

                            {{--<div class="topbar-language rs1-select2" >--}}
                                {{--<select class="selection-1" name="time">--}}
                                    {{--<option>USD</option>--}}
                                    {{--<option>EUR</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            <div class="topbar-language rs1-select2 pl-2" style="font-size: 1.1rem">
                                    <a class="topbar-email" style="{{LaravelGettext::getLocale() == 'pl_PL' ? 'font-weight: bold; color: #e3342f' : ''}}" href="/lang/pl_PL">{{_i('Polski')}}</a>
                                    <a class="topbar-email" style="{{LaravelGettext::getLocale() == 'en_US' ? 'font-weight: bold; color: #e3342f' : ''}}" href="/lang/en_US">{{_i('Angielski')}}</a>
                            </div>
                        </div>
                    </li>

                    <li class="item-menu-mobile {{Request::is('/') ? 'sale-noti' : ''}}">
                        <a href="{{route('home')}}">{{_i('Strona główna')}}</a>
                    </li>

                    <li class="item-menu-mobile {{Request::is('books*') ? 'sale-noti' : ''}}">
                        <a href="{{route('books.index')}}">{{_i('Sklep')}}</a>
                        <ul class="sub_menu">
                            @foreach((App\Attributes::all()->toArray()) as $attribute)
                                <li><a href="{{route('books.attributes', $attribute['id'])}}">{{_i($attribute['name'])}}</a></li>
                            @endforeach

                        </ul>
                    </li>

                    <li class="item-menu-mobile {{Request::is('about') ? 'sale-noti' : ''}}">
                        <a href="{{route('about')}}">{{_i('O nas')}}</a>
                    </li>

                    <li class="item-menu-mobile {{Request::is('contact') ? 'sale-noti' : ''}}">
                        <a href="{{route('contact')}}">{{_i('Kontakt')}}</a>
                    </li>

                </ul>
            </nav>
        </div>
        <!-- Container Selection1 -->
        <div id="dropDownSelect1"></div>

    </header>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });

    </script>
    @endsection
