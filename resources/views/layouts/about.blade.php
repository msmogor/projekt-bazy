@extends('layouts.app')

@section('content')
    <!-- Title Page -->
    <section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/new/about.jpg);">
        <h2 class="l-text2 t-center">
            {{_i('O nas')}}
        </h2>
    </section>

    <!-- content page -->
    <section class="bgwhite p-t-66 p-b-38">
        <div class="container">
            <div class="row">
                <div class="col-md-4 p-b-30">
                    <div class="hov-img-zoom">
                        <img src="images/new/about-cingciong.jpeg" alt="IMG-ABOUT">
                    </div>
                </div>

                <div class="col-md-8 p-b-30">
                    <h3 class="m-text26 p-t-15 p-b-16">
                        {{_i('O nas')}}
                    </h3>

                    <p class="p-b-28">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras at est ac mi fringilla luctus non vitae tortor. Quisque finibus cursus sem, nec euismod dui ornare vel. Maecenas vestibulum nisi id sem tincidunt pretium. Sed quam velit, ultrices non dapibus eu, mattis nec urna. Morbi lobortis orci eget semper cursus. Aliquam varius feugiat dolor, sit amet ultrices mi cursus finibus. Curabitur placerat lectus neque, vitae luctus purus dictum sit amet. Donec non fermentum nibh. Curabitur eros urna, viverra pharetra tellus vel, porta sollicitudin turpis. Aenean porta placerat tellus a interdum.
                    </p>

                    <p class="p-b-28">
                        Mauris tristique, mauris at sollicitudin consequat, nibh sapien sollicitudin dolor, non vestibulum justo nisl pulvinar nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam tempus nunc eget turpis egestas tempor. Integer eleifend nisl non odio dictum luctus. Etiam tempus diam nec elit porta, id facilisis nibh pellentesque. Donec convallis libero eu est commodo, vitae consequat orci auctor. Cras gravida neque justo, sit amet sollicitudin quam fermentum id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur leo sem, molestie at ante at, ullamcorper viverra elit. Nunc hendrerit augue id maximus fringilla.
                    </p>

                    <div class="bo13 p-l-29 m-l-9 p-b-10">
                        <p class="p-b-11">
                            Creativity is just connecting things. When you ask creative people how they did something, they feel a little guilty because they didn't really do it, they just saw something. It seemed obvious to them after a while.
                        </p>

                        <span class="s-text7">
							- Steve Job’s
						</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection

@section('scripts')

    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });

        $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect2')
        });
    </script>
    <!--===============================================================================================-->
    @endsection