<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">--}}

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    {{--Layout--}}
    <link rel="stylesheet" type="text/css" href="{{url('/vendor').'/bootstrap/css/bootstrap.min.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/fonts').'/font-awesome-4.7.0/css/font-awesome.min.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/fonts').'/themify/themify-icons.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/fonts').'fonts/Linearicons-Free-v1.0.0/icon-font.min.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/fonts').'/elegant-font/html-css/style.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor').'/animate/animate.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor').'/css-hamburgers/hamburgers.min.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor').'/animsition/css/animsition.min.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor').'/select2/select2.min.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor').'/daterangepicker/daterangepicker.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor').'/slick/slick.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor').'lightbox2/css/lightbox.min.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor').'/noui/nouislider.min.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/css').'/util.css'}}">
    <link rel="stylesheet" type="text/css" href="{{url('/css').'/main.css'}}">

</head>
<body class="animsition">
    <div id="app">
        @include('layouts.navbar2')
        @yield('navbar')
        <main>
            @yield('content')
        </main>
        @include('layouts.footer')
        @yield('footer')
    </div>
    @yield('scripts')
    <script src="{{url('js').'/main.js'}}"></script>
    <script type="text/javascript">
        // $(".delete_book").each(function (){
        //     $(this).on('click', function(){
        //         console.log("xddd");
        //         let book_id = $(this).parent().attr('id');
        //         $('#'+book_id).remove();
        //         let items_count_cart = parseInt($('.header-icons-noti').html());
        //         $('.header-icons-noti').html(--items_count_cart);
        //         book_id = book_id.split('_');
        //         book_id = book_id[1];
        //         axios.post('/cart/destroy',{
        //             data:{
        //                 book_id: book_id
        //             }
        //         }).then(()=>{
        //
        //         });
        //     })
        // });
    </script>
</body>
</html>
