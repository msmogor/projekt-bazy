<div class="pagination flex-m flex-w p-t-26">
@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled"><span>{{_i('Poprzednie')}}</span></li>
        @else
            <li class=""><a href="{{ $paginator->previousPageUrl() }}" rel="prev">{{_i('Poprzednie')}}</a></li>
        @endif

        @if($paginator->currentPage() > 3)
            <li class="hidden-xs"><a href="{{ $paginator->url(1) }}" class="item-pagination flex-c-m trans-0-4">1</a></li>
        @endif
        @if($paginator->currentPage() > 4)
            <li class="item-pagination flex-c-m trans-0-4"><span>...</span></li>
        @endif
        @foreach(range(1, $paginator->lastPage()) as $i)
            @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                @if ($i == $paginator->currentPage())
                    <li class="item-pagination flex-c-m trans-0-4 active-pagination"><span>{{ $i }}</span></li>
                @else
                    <li><a href="{{ $paginator->url($i) }}" class="item-pagination flex-c-m trans-0-4">{{ $i }}</a></li>
                @endif
            @endif
        @endforeach
        @if($paginator->currentPage() < $paginator->lastPage() - 3)
            <li class="item-pagination flex-c-m trans-0-4"><span>...</span></li>
        @endif
        @if($paginator->currentPage() < $paginator->lastPage() - 2)
            <li class="hidden-xs"><a href="{{ $paginator->url($paginator->lastPage()) }}" class="item-pagination flex-c-m trans-0-4">{{ $paginator->lastPage() }}</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">{{_i('Następne')}}</a></li>
        @else
            <li class="disabled"><span>»</span></li>
        @endif
    </ul>
@endif
</div>
