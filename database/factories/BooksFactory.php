<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker){
    return [

            'stock'=>$faker->numberBetween($min = 5, $max = 100),
            'name' => $faker->sentence($nbWords = $faker->numberBetween($min = 3, $max = 6), $variableNbWords = true),
            'description' => $faker->text,
            'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 50),
            'available' => $faker->numberBetween($min = 0, $max = 1),
            'is_new' => $faker->boolean,
            'is_sale' => $faker->boolean,
            'img'=>'images/new/book-placeholder.jpg',
            'wrote_at' => $faker->date($format = 'Y-m-d', $max = 'now')
          ];
});
