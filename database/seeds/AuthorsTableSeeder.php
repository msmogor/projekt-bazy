<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\DataType;
use App\Author as Author;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $dataType = $this->dataType('slug', 'authors');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'authors',
                'display_name_singular' => _i('Author'),
                'display_name_plural'   => _i('Authors'),
                'icon'                  => 'voyager-study',
                'model_name'            => 'App\\Author',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        $menu = Menu::where('name', 'custom-admin')->firstOrFail();
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => _i('Authors'),
            'url'     => '',
            'route'   => 'voyager.authors.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-study',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 6,
            ])->save();
        }


        factory(Author::class,10)->create();
    }

    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }

    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
