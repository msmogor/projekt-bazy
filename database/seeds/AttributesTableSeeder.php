<?php

use Illuminate\Database\Seeder;

class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Attributes::create([
            'name' => 'Popularne',
        ]);
        App\Attributes::create([
            'name' => 'Najnowsze',
        ]);
        App\Attributes::create([
            'name' => 'Wyróżnione',
        ]);
    }
}
