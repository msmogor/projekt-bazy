<?php

use App\Attributes as Attributes;
use Illuminate\Database\Seeder;
use App\Book as Book;
use App\Categories as Category;
use App\Author as Author;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

use TCG\Voyager\Models\Page;
use TCG\Voyager\Models\Permission;

/**
 * Te nieużywane klasy do wywalenia??? ^
 */

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $dataType = $this->dataType('slug', 'books');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'books',
                'display_name_singular' => _i('Book'),
                'display_name_plural'   => _i('Books'),
                'icon'                  => 'voyager-book',
                'model_name'            => 'App\\Book',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        $menu = Menu::where('name', 'custom-admin')->firstOrFail();
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => _i('Books'),
            'url'     => '',
            'route'   => 'voyager.books.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-book',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 7,
            ])->save();
        }

        $cat_count = Category::count();
        $aut_count = Author::count();
        $att_count = Attributes::count();
        factory(Book::class, 120)->create()->each(function($book) use($cat_count,$aut_count,$att_count) {
            $category = Category::where('id', rand(1,$cat_count))->first();
            $author = Author::where('id', rand(1,$aut_count))->first();
            $attribute = Attributes::where('id', rand(1,$att_count))->first();
            $book->categories()->attach($category);
            $book->authors()->attach($author);
            $book->attributes()->attach($attribute);
        });

    }

    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }

    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
