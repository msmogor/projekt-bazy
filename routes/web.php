<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Xinax\LaravelGettext\Facades\LaravelGettext;

Auth::routes();

//Book
Route::get('/', 'HomeController@index')->name('home');
Route::get('/books','BooksController@index')->name('books.index');
Route::get('/books/popular', 'BooksController@showPopular')->name('books.popular');
Route::get('/books/newest', 'BooksController@showNewest')->name('books.newest');
Route::get('/books/featured', 'BooksController@showFeatured')->name('books.featured');
Route::get('/books/attributes/{id}', 'BooksController@showByAttribute')->name('books.attributes');
Route::get('/books/category/{id}', 'BooksController@showByCategory')->name('books.category');
Route::get('/books/{id}/', 'BooksController@show')->name('books.show');

//User
Route::get('/user', 'UserController@index')->name('user.index');
Route::group(['prefix' => 'user'], function () {
    Route::get('/favorites', 'FavoritesController@index')->name('user.favorites');
    Route::get('/favorites/delete/{id}', 'FavoritesController@destroy')->name('favorites.delete');
    Route::get('/edit', 'UserController@edit')->name('user.edit');
    Route::post('/update','UserController@update')->name('user.update');

});
//Cart
Route::get('/cart', 'CartController@index')->name('cart.index');

//User Cart
Route::post('/cart/store', 'UserCartController@store')->name('cart.store');
Route::get('/cart/destroy/{id}', 'UserCartController@destroy')->name('cart.destroy');


Route::get('/about', function (){
    return view('layouts.about', compact('attributes'));
})->name('about');

Route::get('/contact', function (){
    return view('layouts.contact', compact('attributes'));
})->name('contact');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/lang/{locale}', function ($locale = null){
    LaravelGettext::setLocale($locale);
    $lang = LaravelGettext::getLocale();
    return redirect()->back();
});
